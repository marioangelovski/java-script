// JSON

const classRoomAsJson = `{
    "trainer": "Jonh Doe",
    "assistant": "John Smith",
    "students": [
        "Bob",
        "Chris",
        "Jill",
        "Greg"
    ],
    "academy": "SEDC"
}`;

console.log(classRoomAsJson);

// const classRoomAsLiteral = JSON.parse(classRoomAsJson); //JSON to object(litteral)

const classRoomAsLiteral = {
    trainer : "Jonh Doe",
    assistant : "John Smith",
    student: [
        "Bob",
        "Chris",
        "Jill",
        "Greg"
    ],
    academy: "SEDC"
};
console.log(classRoomAsLiteral)

const classRoomAsJson2 = JSON.stringify(classRoomAsLiteral); // object to JSON
console.log(classRoomAsJson2);

// fetch API
fetch("students.json")
    .then(response => response.json())
    .then(data => console.log(data));


    document.querySelector("#cat-facts-btn").addEventListener("click", function(){
        fetch("https://cat-fact.herokuapp.com/facts")
            .then(response => response.json())
            .then(data => printCatFacts(data));
    });

    function printCatFacts(catFacts){
        const element = document.querySelector("#cat-facts-list");
        let htmlAsString = ""

        for(let item of catFacts){
            htmlAsString += `<li> ${item.text}</li>`;
        }
        element.innerHTML = htmlAsString;
    }


    // dog
    function getRandomDog(){
        fetch("https://dog.ceo/api/breeds/image/random")
            .then(response => response.json())
            .then(data => printDogImage(data.message))
    }
    document.querySelector("#dog-random-btn").addEventListener("click", getRandomDog);

    function printDogImage(imgUrl){
        const element = document.querySelector("#dog-random-wrapper")
        element.innerHTML = `<img src="${imgUrl}" width = "250px" />`
    }
    // setInterval(function(){
    //     getRandomDog();
    // },5000);


    document.querySelector("#aboutus-link").addEventListener("click", function(){
        fetch("pages-content.json")
        .then(response => response.json())
        .then(data => printContent(data));

    });

    function printContent(data){
        const element = document.querySelector("#page-content");
        element.innerHTML = `<h3> ${data.page}</h3>
        <p> ${data.content} </p>
        <p> ${data.content2} </p>
        <img src = "${data.imgUrl}"/>`

    }


    document.querySelector("#job-btn").addEventListener("click", function(){
        fetch("http://api.icndb.com/jokes/random")
            .then(response => response.json())
            .then(data => printJob(data));
    });

    function printJob(data){
        const element = document.querySelector("#job-list");
        element.innerHTML =  `<li> ${data}</li>`
    }
