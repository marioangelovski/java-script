// jQuery ready statement
$(function() {
    // body of the function
    console.log("the Dom is ready");
    const allTrs =  $(".main-table tr");//selects all tr in main-table class
    console.log("all trs", allTrs)


    const evenP = $("p:even").addClass("p-even");
    console.log("even p", evenP);
    const oddP = $("p:odd").addClass("p-odd");
    console.log("odd", oddP);

    const first4ListItems = $(".menu-list li:lt(4)");
    const last3ListItems = $("menu-list li:gt(3)");
    
});

