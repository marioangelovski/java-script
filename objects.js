//literal notation

const hotelRadika = {
	name: "radika", // propery, key:name, "radika": value
	rooms: 40,
	booked: 20,
	gym: true,
	roomType: ["twin", "double", "single"],
   
   
	checkAvailability: function(){ //method
		return this.rooms - this.booked;
	}
};

console.log(hotelRadika.name);
console.log(hotelRadika.rooms);// dot notation
console.log(hotelRadika["rooms"]);//square
console.log(hotelRadika.checkAvailability());

const shape = {
	width: 600
};
console.log(shape);
shape["height"] = 400;
console.log(shape);


var hotel = {
	name: "Hotel 5",
	rooms: 45,
	booked: 10
};

const hotelName = hotel.name;
console.log ("initial hotel name", hotelName);
hotel.name = "Hotel Star 5";
console.log("new hotel name ", hotel.name)
hotel.gym = false;
console.log("hotel", hotel);
delete hotel.gym;
console.log("hotel", hotel)



// 2 constructor notation
const hotelR = new Object();
hotelR.name = "radika2";
hotelR.rooms = 40;
hotelR.booked = 15;
hotelR.checkAvailability = function(){
	return this.rooms - this.booked;
}



// 3 object with fucntion constructor

function Hotel(name, rooms, booked, gym, roomType){
	this.name = name;
	this.rooms = rooms;
	this.booked = booked;
	this.gym = gym;
	this.roomType = roomType;

	console.log("this", this);

	this.checkAvailability = function(){
		return this.rooms - this.booked;
	}
};
// instances
const hotelRadika2 = new Hotel("Radika", 40, 20, true, ["twins","single","double"]);
hotelRadika2.checkAvailability();//20


const hotel5 = new Hotel("Hotel 5", 100, 90, false, ["single"]);
hotel5.checkAvailability();//10

console.log(hotel5.checkAvailability());

// task 01
// apply for a job form
const name = document.querySelector("#name");
const lastname = document.querySelector("#lastname");
const education = document.querySelector("#education");
const age = document.querySelector("#age");
const sendBtn = document.querySelector("#send-btn");

let persons = [];
sendBtn.addEventListener("click", function(e){
	// const name = name.value;
	// const lastname = lastname.value;
	// const education = education.value;
	// const age = age.value; 
	e.preventDefault();
	const person = new Person(name.value, lastname.value, education.value, age.value);
	persons.push(person);
	console.log("person", person);

});

function Person(name,lastname, education, age){
	this.name = name;
	this.lastname = lastname;
	this.education = education;
	this.age = age;
}

const student = {
	name : "David Rayy",
	sclass : "VI",
	rollno : 12,
	
	studentPrintData: function(){
		 return `"${this.name}","${this.sclass}", "${this.rollno}"`;

	 }

};

console.log(student.studentPrintData());


const skoda = {
	model : "Octavia",
	color : "White",
	fuel : 50,
	year: 2010,
	distance: 300,
	fuelConsumption : 7,
	
	
	calculateConsumption: function(){
		return ` ${(this.fuelConsumption*this.distance)/100} ${this.fuel}`;
	}
}

console.log(skoda.calculateConsumption());



function Car(model, color, year, fuel, fuelConsumation){
	this.model = model;
	this.color = color;
	this.year = year;
	this.fuel = fuel;
	this.fuelConsumation = fuelConsumation;
	calculateFuelForDistance = function(distance){
		return ` ${(this.fuelConsumption*this.distance)/100} ${this.fuel}`;
	};
	this.printInfo = function(){
		return `the car ${this.model} is ${new Date().getFullYear() - this.year}old and has the color ${this.color}`
	}
};



//car with html
const model = document.querySelector("#model");
const color = document.querySelector("#color");
const year = document.querySelector("#year");
const fuel = document.querySelector("#fuel");
const fuelConsumation = document.querySelector("#fuel-consumation");
const distance = document.querySelector("#distance");
const createBtn = document.querySelector("#create-btn");
const printDetialsBnt = document.querySelector("#print-details-btn");
const calculateBtn = document.querySelector("#calculate-btn");
const printDetails = document.querySelector("#p-details");
const printCalculations = document.querySelector("#p-calculations");

createBtn.addEventListener("click", function(e){
	e.preventDefault();
	const car = new Car(model.value, color.value, year.value, fuel.value, fuelConsumation.value);
	console.log(car)
	printDetails.innerText = car.printInfo();
});
calculateBtn.addEventListener("click", function(e){
	e.preventDefault();
	car.calculateFuelForDistance(distance.value);
	printCalculations.innerText = "";
});