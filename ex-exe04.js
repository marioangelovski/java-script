const register = document.querySelector(".register");
const login = document.querySelector(".login");
const fullName = document.querySelector("#fname");
const username = document.querySelector("#usern");
const pass = document.querySelector("#pass");
const repass = document.querySelector("#repass");
const createBtn = document.querySelector("#create-btn");
const result = document.querySelector("#result");

//login
const result2 = document.querySelector("#result2");
const loginBtn = document.querySelector("#login-btn");
const loginUname = document.querySelector("#l-usern");
const loginPass = document.querySelector("#l-pass");

users = [];
createBtn.addEventListener("click", function(){
    if(getFormValidationErrors().length > 0){
        printErrors(getFormValidationErrors());
    } 
    else {
        result.innerHTML = `<li style="color: green"> Success. <button id="new-login-btn"> Login </button> </li>`;
        users.push(username.value, pass.value);
    };
});


function getFormValidationErrors() {
    let errors = [];
    if (fullName.value.length < 3) {
        errors.push('Fullname must contain more than 2 characters');
    }
    if (username.value.length < 3) {
        errors.push('Username must contain more than 2 characters');
    }
    if (pass.value.length < 3 || (pass.value !== repass.value)) {
        errors.push('Password error');
    }
    return errors.length > 0 ? errors : [];
}

function printErrors(arr) {
    result.innerHTML = "";
    for (let item of arr) {
        result.innerHTML += `<li> ${item} </li>`;
    }
}

// event delegation
document.addEventListener("click", (e) => {
    if (e.target.id === "new-login-btn") {
        register.style.display = "none";
        login.style.display = "block";
    }
});


function isUserExist(username, pass) {
    return users[0] === username && users[1] === pass;
}

loginBtn.addEventListener("click", () => {
   if (isUserExist(loginUname.value, loginPass.value)) {
       result2.innerHTML = `<li> Welcome, ${loginUname.value} </li>`;
   } else {
       result2.innerHTML = `<li> Wrong credentials </li>`;
   }
});





