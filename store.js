function Store(name) {
    this.name = name;
    this.products =[];
    this.shopingCartProducts =[];
    this.addProduct = function(product){
        this.products.push(product);
    }

    this.listProducts  = function (){
        const element = document.querySelector("#products");
        let html ="";
        let index = 0;
        for(let item of this.products){
            html += `<li data-index="${index}">
                    <h4>${item.name}</h4>
                    <div><img src="${item.imageUrl}" width="150"></div>
                    <div>${item.price}</div>
                    <div>${item.description}</div>
                    <div>Compare<input type = "checkbox" class="compare-chk"></div>
                    <div><button class="add-to-cart-btn"> add to cart </button></div>
                    </li>`
                    index++;
        }
        element.innerHTML = html;
        console.log(this.products);
    }

    this.listProductsInShoppingCart= function(){
        const element = document.querySelector("#shoping-cart")
        let htmlToAdd = "";
        for(let item of this.shopingCartProducts){
            htmlToAdd += `<li>
                ${item.name} - ${item.price}
            </li>`;
        }
        element.innerHTML = htmlToAdd;
    }

    this.getPoductByIndex = function(index){
        let product = this.products[index];
        if(product){
            return product;;

        }else{
            return false;
        }
    }
 
    this.addToCart = function(product){
        this.shopingCartProducts.push(product);
        this.listProductsInShoppingCart();
    }
    this.compareProducts = function(product1, product2){
        const element = document.querySelector("#compare");
        element.innerHTML = `<table border="1">
        <tr>
            <th> ${product1.name} </th>
            <th> ${product2.name} </th>
        </tr>
        <tr>
            <td> <img src="${product1.imageUrl}" width="60" /> </td>
            <td> <img src="${product2.imageUrl}" width="60" /> </td>
        </tr>
        <tr>
            <td> ${product1.price} denari </td>
            <td> ${product2.price} denari </td>
        </tr>
        <tr>
            <td> ${product1.description} </td>
            <td> ${product2.description} </td>
        </tr>
        </table>
        `
    }
}

function Product(name, price, imageUrl, description){
    this.name = name;
    this.price = price;
    this.imageUrl = imageUrl;
    this.description = description;
}

const myStore = new Store("sedc store");
const tvSamsung = new Product("samsung", 26500, "https://bit.ly/3qQC6aM", "FHD 52' smartTV");
const tvLg = new Product("lg", 50000, "https://bit.ly/38NWyCR", "4K, 15w soundbar, smartTV");
const tvPhilips = new Product("Philips ph600", 16500, "https://bit.ly/3lmzeky", "UHD with led backlight ")
const tvSony = new Product ("Sony Sn001", 68000, "https://bit.ly/3qXwhrP", "4k extra image quality");


myStore.addProduct(tvSamsung);
myStore.addProduct(tvLg);
myStore.addProduct(tvPhilips);
myStore.addProduct(tvSony);

myStore.listProducts();


document.addEventListener("click", function(e){
    if(e.target.classList.contains("add-to-cart-btn")){
        const productIndex = parseInt(e.target.closest("li").getAttribute("data-index"));
        const product = myStore.getPoductByIndex(productIndex);
        if(product){
        myStore.addToCart(product);
    }
    };

});
let productsToCompare = [];

document.addEventListener("input", function(e){
    if(e.target.classList.contains("comapre-chk")){
    const productIndex = parseInt(e.target.closest("li").getAttribute("data-index"));
    const product = myStore.getPoductByIndex(productIndex);
        
    productsToCompare.push(product);
    if(productsToCompare.length <3){
        productsToCompare.push(product);
    }
    if(productsToCompare.length === 2){
        myStore.compareProducts(productsToCompare[0], productsToCompare[1]);
    }

    }
})

function uncheckCompareInputs(){
    const inputs = document.querySelectorAll("")
}