$(function(){
    let id = 0;

    function App(){
        this.students = [];
        this.searchedStudent = [];

        this.addStudent = function(student){
            this.students.push(student)
            app.clearStudent();
        }
        this.printStudents = function(){
            let htmlToAdd = "";
            const actionsBtn = `<button class="edit-btn">edit</button>
                                <button class="delete-btn">delete</button>`;
            for(let item of this.students){
                htmlToAdd += `<li id=${item.id}>  [${item.index}] ${item.firstname} ${item.lastname} visits [${item.subjects}] subjects ${actionsBtn} </li>`
            }
            $("#student-list").html(htmlToAdd);
        }


        this.deleteStudent = function(studentId){
            // const student = this.students[studentId];
            const student = this.students.find(x=> x.id === studentId);
            
            
            if(student){
                const index = this.students.indexOf(student);
                this.students.splice(index, 1);
                this.printStudents();
            }
        }


        this.clearStudent = function(){
            $("#firstname").val("");
            $("#lastname").val("");
            $("#index").val("");
            $("#email").val("");
            $("#subjects").val("");
            
        }
        this.searchByName = function(name){
            const student = this.students.find(x => x.firstname.toLowerCase() === name.toLowerCase());
            if(student){
                console.log(student);
                this.searchedStudent.push(student);
                this.printSearch();
            }
        }
        this.printSearch = function(){
            let htmlToAdd = "";
            for(let item of this.searchedStudent){
                htmlToAdd += `<li>${item.firstname} ${item.lastname}</li>`
            }
            $("#search-list").html(htmlToAdd);
        }
    }

    function Student(firstname,lastname,index,email,subjects){
        this.id = id++
        this.firstname = firstname;
        this.lastname = lastname;
        this.index = index;
        this.email = email;
        this.subjects = subjects;
    }
    // const st1 = new Student("John", "doe", "2021-15", "johndoe@gmail.com", ["Html and Css", "JavaScript and JQuery", "UX/UI"]);
    // const st2 = new Student("John", "doe", "2021-15", "johndoe@gmail.com", ["Html and Css", "JavaScript and JQuery", "UX/UI"]);
    // app.addStudent(st1);
    // app.addStudent(st2);
    // app.printStudents();

    const app = new App();
    $("#add-btn").on("click", function(e){
        e.preventDefault();
        const firstname = $("#firstname").val();
        const lastname = $("#lastname").val();
        const index = $("#index").val();
        const email = $("#email").val();
        const subjects = $("#subjects").val();

        app.addStudent(new Student(firstname, lastname, index, email, subjects));
        app.printStudents();
    });
    $("#clear-btn").on("click", function(e){
        e.preventDefault();
       app.clearStudent();
    });
    $(document).on("click", ".delete-btn", function(){
        const index = $(this).parents("li").attr("id");
        app.deleteStudent(parseInt(index));
    });

    $(document).on("input", function(){
        const name = $("#search").val();
        app.searchByName(name);
    });

});