// step01
// select the element
// 
// 

const clickBtn = document.getElementById("click-me-btn");


function sayHello(){
    alert("Hello!");
}


// step 02

const click2Btn = document.getElementById("click-2");
click2Btn.onclick = function(){
    alert("you just clicked button 2")
}

// click2Btn.onclick = sayHello

// step 03
const click3Btn = document.getElementById("click-3");

click3Btn.addEventListener("click", sayHello, false);

// step 04

const txt1 = document.getElementById("txt-1");
const txt1Result = document.getElementById("txt-1-result");

txt1.addEventListener("blur", function(event){
    console.log(event);
    // event.target.value go zima value
    txt1Result.innerText = `Zdravo ${txt1.value}`;
    // txt1Result.innerText = `Zdravo ${e.target.value}`;
});

// box-1
const box1 = document.getElementById("box-1");
box1.addEventListener("mousemove", function(){
    box1.style.backgroundColor = "orange";
});

box1.addEventListener("mouseleave", function() {
    box1.style.backgroundColor = "limegreen";
});

// username check
// 1 select element
const username = document.getElementById("username");
const usernameResult = document.getElementById("username-result");
// implement the logic
// default parametar value => minLength = 3
function checkUsername(username, minLenght = 3){
    let result = "";
    if(username.length >= minLenght){
        result = `${username} is valid`
        
    }
    else{
        result = `${username} is not valid, the min chars is${minLenght}`
    }
    return result;
    
}
// test logic
// checkUsername("ad", 3); // not valid
// checkUsername("John", 3); //valid

// 4 event and event handling

username.addEventListener("blur", function() {
    usernameResult.innerText = checkUsername(username.value, 3);
});


// multiple element event

const btns = document.getElementsByClassName("btn");
for(let btn of btns){
    btn.addEventListener("click", function(){
    
    alert("you clicked me")
});
};


const paragraph1 = document.querySelector("#paragraph-1");
const changeStyleBtn = document.querySelector("#change-style-btn");
const colorElement = document.querySelector("#color-string");
const fontSizeElement = document.querySelector("#font-size")
const fontWeightElement = document.querySelector("#font-weight");

changeStyleBtn.addEventListener("click", changeStyle);

function changeStyle(){
    paragraph1.style.color = colorElement.value; 
    paragraph1.style.fontSize = fontSizeElement.value;
    paragraph1.style.fontWeight = fontWeightElement.value;
}

// task 02

const registerBtn = document.querySelector("#register");
const firstName = document.querySelector("#name");
const eMail = document.querySelector("#mail");
const password = document.querySelector("#password");

const paragraphRegister = document.querySelector("#paragraph-register")

registerBtn.addEventListener("click", register);

function register(){
    paragraphRegister.innerHTML = `${firstName.value} - ${eMail.value} - ${password.value}`;
}

// task 03

window.addEventListener("resize", function(event){
    console.log("resize", event);
    console.log(event.target.innerWidth);
    console.log(event.target.innerHeight);
    document.querySelector("#resize-result").innerText = `${event.target.innerHeight} x ${event.target.innerWidth} `;   

})







