let myApp = document.getElementById("app");
let tittleDiv = document.getElementById("title");
let contentDiv = document.getElementById("content");

let students = ["Bob Bobsky", "Jill Cool", "John Doe", "Jane Sky"];
let subjects = ["Math", "English", "Science", "Sport"];
let grades = ["A", "B", "A", "C"];

// math: A
// english: B


function printGrades(subjects, grades, element){
    element.innerHTML += "<ul>";
    for(let i=0; i<subjects.length; i++){
        element.innerHTML +=`<li> ${subjects [i]}: ${grades [i]} </li>`;
    }
    element.innerHTML += "</ul>";
}

// printGrades(subjects, grades, contentDiv);

function printStudents(students, element){
    let html = ""
    html += "<ol>"
    for(let student of students){
        html += `<li>${student} </li>`
    }
    html += "</ol>"
    element.innerHTML += html;

}

// printStudents(students, contentDiv);

function academyPanel(personType, name){
    if(personType === "student"){
        tittleDiv.innerHTML += `<h1> Hello, ${name} </h1>`;
        printGrades(subjects, grades, contentDiv);
    }
    else if(personType === "teacher"){
        tittleDiv.innerHTML += `<h1> Hello teacher, ${name} </h1>`
        printStudents(students, contentDiv);
    }
    else{
        tittleDiv.innerHTML = "<h3 style = 'color: red'>unexpected input</h3>"
    }
}

// academyPanel("student", "John");
// academyPanel("nesto", "nesto")
// academyPanel("teacher", "Doe")
let personType = prompt("Are you a student or a teacher?");
let name = prompt("What is your name")
academyPanel(personType, name);